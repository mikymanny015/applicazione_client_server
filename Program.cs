using System;
using System.Net.Sockets;
using System.Threading;

namespace AppC_S
{
    class Program
    {
        static void Main(string[] args)
        {
            Client c = new Client();
            Server s = new Server();
            Thread ths = new Thread(new ThreadStart(s.Attivitta));
            Thread thc = new Thread(new ThreadStart(c.Attività));
            thc.Name = "Cliente";
            ths.Name = "Servente";
            ths.IsBackground = true;
            ths.Start();
            while (!ths.IsAlive) ;// attendo partenza server 
            thc.Start();
            Thread.Sleep(10);// se thread start messo dopo questa istruzione si aumenta sleep a 100000, ecezzione mancata risposta 
            thc.Join();
            Console.WriteLine("Premere un tasto per continuare");
            Console.ReadKey();
        }
    }
}